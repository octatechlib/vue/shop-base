# vue-shop

[![State badge](https://img.shields.io/badge/Release-unreleased-orange.svg)](https://gitlab.com/octatechlib)
[![Version badge](https://img.shields.io/badge/Version-x.x-blue.svg)](https://gitlab.com/octatechlib)
[![Last stable badge](https://img.shields.io/badge/Last_stable-x.x-green.svg)](https://gitlab.com/octatechlib)
[![Licence badge](https://img.shields.io/badge/Licence-GPL-1f425f.svg)](https://gitlab.com/octatechlib)

In progress

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Playwright](https://playwright.dev)

```sh
# Install browsers for the first run
npx playwright install

# When testing on CI, must build the project first
npm run build

# Runs the end-to-end tests
npm run test:e2e
# Runs the tests only on Chromium
npm run test:e2e -- --project=chromium
# Runs the tests of a specific file
npm run test:e2e -- tests/example.spec.ts
# Runs the tests in debug mode
npm run test:e2e -- --debug
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
