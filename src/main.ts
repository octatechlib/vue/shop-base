//import './assets/main.css'

import { createApp, watch } from 'vue'
import { createPinia } from 'pinia'

import { vuetify } from '@/plugins/vuetify';
import i18n from '@/locales/i18n';
import router from './router/index'

//import App from './AppOld.vue'
//import router from './router/index_old'

import App from './App.vue'
import ApiClient from '@/clients/ApiClient';
import TranslationKey from './components/language/TranslationKey.vue';
import './assets/main.scss';


// Vuetify
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/styles';
import './assets/custom.css';

import "material-design-icons-iconfont/dist/material-design-icons.min.css";

import { StoreFactory } from '@/stores';
import type { StoreFactories } from '@/types/stores';

import { ObjectHelper } from '@/helpers/ObjectHelper';

// const configStore = StoreFactory.configStore;
// const storedLocale = configStore.locale;

const app = createApp(App)
const pinia = createPinia();

declare module 'pinia' {
  export interface PiniaCustomProperties {
    apiClient: ApiClient;
  }
}

pinia.use(() => ({
  apiClient: new ApiClient(),
}));

app.use(createPinia())

app.use(router)
app.use(i18n);
app.use(vuetify);

const api = new ApiClient();

app.component('TranslationKey', TranslationKey as any);

app.provide('api', api);
app.mount('#app')


const storageData = JSON.parse(localStorage.getItem('piniaState') as any);

if (storageData) {
  pinia.state.value = storageData;
}

watch(
  pinia.state,
  (state: any) => {
    localStorage.setItem('piniaState', JSON.stringify(state));
  },
  { deep: true }
);

const storeFactory: StoreFactory & StoreFactories = new StoreFactory() as StoreFactory & StoreFactories;
const objectHelper: ObjectHelper = new ObjectHelper();

export { i18n, storeFactory, objectHelper };
