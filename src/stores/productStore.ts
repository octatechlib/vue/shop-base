import { defineStore } from 'pinia';
import router from '@/router';
import axios, { type AxiosResponse } from 'axios';
import { i18n } from '@/locales/i18n';
import type { ProductState } from '@/types/stores/states';
import type { IndexLinksResponse, IndexMetaResponse } from '@/types/clients/Response';
import { StoreFactory } from '@/stores/index';
import type { Product, ProductIndexResponse } from '@/types/stores/productStoreTypes'

const { t: translate } = i18n;

export const useProductStore = defineStore('productStore', {
  state: (): ProductState => {
    return {
      products: [] as Product[],
      product: {} as Product,
      meta: {} as IndexMetaResponse,
      links: {} as IndexLinksResponse,
    };
  },

  getters: {
    getProducts(state: ProductState) {
      return () => state.products;
    },

    getProduct(state: ProductState) {
      return () => state.product;
    },

    getProductById(state: ProductState) {
      return (ProductId: number) =>
        state.products.find((Product: Product): boolean => {
          return Product.id === ProductId;
        });
    },
  },

  actions: {

    async setProduct(Product: Product): Promise<void> {
      this.product = Product;
    },

    async create(data: any): Promise<false | Product> {
      return this.apiClient.productEndpoint
        .create(data)
        .then((response: AxiosResponse): Product => {
          StoreFactory.toastStore.success(
            translate('global.label.new') + ' ' + translate('products.product'),
            data.Product.first_name + ' ' + data.Product.last_name,
            {
              timeout: 15000,
            }
          );

          return response.data.data;
        })
        .catch((error: any): false => {
          StoreFactory.toastStore.errorResponseToast(error, true);
          return false;
        });
    },

    async update(id: number, data: any): Promise<boolean> {
      return this.apiClient.productEndpoint
        .update(id, data)
        .then((): boolean => {
          StoreFactory.toastStore.success(
            translate('products.update.title'),
            translate('products.update.message', {
              name: `${data.first_name} ${data.last_name}`,
            }),
            { timeout: 15000 }
          );

          return true;
        })
        .catch((error: any): boolean => {
          StoreFactory.toastStore.errorResponseToast(error, true);
          return false;
        });
    },

    async search(ProductId: number): Promise<any> {
      return this.apiClient.productEndpoint
        .show(ProductId)
        .then((response: AxiosResponse) => {
          return {
            ...response.data.data,
            status: response.status,
          };
        })
        .catch((error: any): any => {
          console.error(error); // TODO: nope
        });
    },

    async search(search: any): Promise<any> {
      const response = await this.apiClient.productEndpoint
        .index(search)
        .then((response: AxiosResponse): ProductIndexResponse => {
          return {
            ...response.data,
            status: response.status,
          };
        })
        .catch((error: any): any => {
          StoreFactory.toastStore.errorResponseToast(error, true);
        });

      if (![200, 201, 204].includes(response.status)) {
        //TODO handle error this.handleError()
        return [];
      }

      this.products = response.data;
      this.meta = response.meta;
      this.links = response.links;

      return { items: this.products, total: this.meta.total };
    },

    async searchById(id: number): Promise<false | Product> {
      return await this.apiClient.productEndpoint
        .show(id)
        .then((response: AxiosResponse): Product => {
          this.product = response.data.data;
          return response.data;
        })
        .catch((error: any): any => {
          StoreFactory.toastStore.errorResponseToast(error, true);
          return false;
        });
    },
  },
});
