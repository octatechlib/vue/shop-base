import { defineStore } from 'pinia';
import { useStorage } from '@vueuse/core';
import { useTailwindColors } from '@/helpers/swatch-generator';
import type { ConfigStoreTypes } from '@/types/stores/configStoreTypes';

export const useConfigStore = defineStore('configStore', {
    state: (): ConfigStoreTypes => ({
        clientSettings: {
            name: 'Octa shop',
        },
        apiValidationSettings: {
            isIgnoringAddressApi: false,
        },
        locale: useStorage('octa-shop/config/locale', 'pl_pl'),
        theme: {
            mode: useStorage('octa-shop/config/theme-mode', 'light'),
            color: {
                primary: '#142340',
                secondary: '#3498db',
                swatch: {
                    primary: useTailwindColors('#ad4444'),
                    secondary: useTailwindColors('#485460'),
                },
            },
        },
    }),

    actions: {
        setSwatchColors(primary: any, secondary: any) {
            this.theme.color.swatch.primary = useTailwindColors(primary);
            this.theme.color.swatch.secondary = useTailwindColors(secondary);
        },
        setThemeMode(mode: string) {
            this.theme.mode = mode;
        },
        setLocale(locale: string) {
            this.locale = locale;
        },
    },

    getters: {
        colorSwatchPrimary(): any {
            return this.theme.color.swatch.primary;
        },
        dark(): any {
            return this.theme.mode === 'dark';
        },
        light(): any {
            return this.theme.mode === 'light';
        },
    },
});
