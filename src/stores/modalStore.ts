import { defineStore } from 'pinia';

export const useModalStore = defineStore('modalStore', {
    state: (): any => {
        return {
            modals: [],
        };
    },
    actions: {
        show(notification: any): void {
            const defaultOptions = {
                id: Math.random(),
                // type: "primary",
                // title: "Notification",
                // icon: "ri-information-fill",
                // message: "This is a notification",
                // timeout: 5000,
                // showClose: true,
            };
            const newModal = { ...defaultOptions, ...notification };

            this.modals.push(newModal);
        },

        removeModal(id: any): void {
            this.modals = this.modals.filter((n: any): boolean => n.id !== id);
        },
    },
});
