import { defineStore } from 'pinia';
import router from '@/router';
import axios, { type AxiosResponse } from 'axios';
import { i18n } from '@/locales/i18n';
import type { UserState } from '@/types/stores/states';
import type { Permission, Role, User, UserIndexResponse } from '@/types/stores/userStoreTypes'
import type { IndexLinksResponse, IndexMetaResponse } from '@/types/clients/Response';
import { StoreFactory } from '@/stores/index';
import { ActivityStatusEnum } from '@/enums/userEnum';
import type { Methods } from '@/enums/apiEnum'

const { t: translate } = i18n;

export const useUserStore = defineStore('userStore', {
    state: (): UserState => {
        return {
            users: [] as User[],
            user: {} as User,
            meta: {} as IndexMetaResponse,
            links: {} as IndexLinksResponse,

            permissions: {} as Permission[],
            current_user: {} as User,

            refresh_token: '',
            roles: {} as Role[],
            token: '',
        };
    },

    getters: {
        getRefreshToken() {
            return '123'; // TODO: load
        },

        getLoggedInUser(state: UserState) {
            return '123'; // TODO: load
        },

        hasPermissionByNameAndMethod(state: AuthUserState): (name: string, method: Methods) => boolean {

            return (name: string, method: Methods) => true;

            // return (name: string, method: Methods) =>
            //   !!state.permissions.find(
            //     (permission: Permission): boolean => permission.name === name && permission.method === method
            //   )?.id;
        },

        hasValidToken() {
            return true; // TODO: load
        },

        getUsers(state: UserState) {
            return () => state.users;
        },

        getUser(state: UserState) {
            return () => state.user;
        },

        getUserById(state: UserState) {
            return (userId: number) =>
                state.users.find((user: User): boolean => {
                    return user.id === userId;
                });
        },

        getCurrentUser(state: UserState) {
            return () => state.current_user;
        },

        getPermissions(state: UserState) {
            return () => state.permissions;
        },


        getToken() {
            return '123'; // TODO: load
        }
    },

    actions: {

        async setUser(user: User): Promise<void> {
            this.user = user;
        },

        async setCurrentUser(user: User): Promise<void> {
            this.current_user = user;
        },

        async create(data: any): Promise<false | User> {
            return this.apiClient.userEndpoint
                .create(data)
                .then((response: AxiosResponse): User => {
                    StoreFactory.toastStore.success(
                        translate('global.label.new') + ' ' + translate('users.user'),
                        data.user.first_name + ' ' + data.user.last_name,
                        {
                            timeout: 15000,
                        }
                    );

                    return response.data.data;
                })
                .catch((error: any): false => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                    return false;
                });
        },

        async update(id: number, data: any): Promise<boolean> {
            return this.apiClient.userEndpoint
                .update(id, data)
                .then((): boolean => {
                    StoreFactory.toastStore.success(
                        translate('users.update.title'),
                        translate('users.update.message', {
                            name: `${data.first_name} ${data.last_name}`,
                        }),
                        { timeout: 15000 }
                    );

                    return true;
                })
                .catch((error: any): boolean => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                    return false;
                });
        },

        async search(userId: number): Promise<any> {
            return this.apiClient.userEndpoint
                .show(userId)
                .then((response: AxiosResponse) => {
                    return {
                        ...response.data.data,
                        status: response.status,
                    };
                })
                .catch((error: any): any => {
                    console.error(error); // TODO: nope
                });
        },

        async searchUsers(search: any): Promise<any> {
            console.log('search', search, this.apiClient)
            const response = await this.apiClient.userEndpoint
                .index(search)
                .then((response: AxiosResponse): UserIndexResponse => {
                    return {
                        ...response.data,
                        status: response.status,
                    };
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });

            if (![200, 201, 204].includes(response.status)) {
                //todo handle error this.handleError()
                return [];
            }

            this.users = response.data;
            this.meta = response.meta;
            this.links = response.links;

            return { items: this.users, total: this.meta.total };
        },

        async searchUserById(id: number): Promise<false | User> {
            return await this.apiClient.userEndpoint
                .show(id)
                .then((response: AxiosResponse): User => {
                    this.user = response.data.data;
                    return response.data;
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                    return false;
                });
        },

        async isActivated(email: string): Promise<any> {
            console.log('isActivated');

            const data = { email: [email] };
            return this.searchUsers({ user: data, page: 1, limit: 1 }).then((result) => {
                this.current_user = result.items[Object.keys(result.items)[0]]

                return this.current_user.activity_status === ActivityStatusEnum.ACTIVE;
            });
        },

        async activate(id: number): Promise<void> {
            this.apiClient.userEndpoint
                .activate(id)
                .then((): any => {
                    StoreFactory.toastStore.success(
                        translate('users.activation.title'),
                        translate('users.activation.message')
                    );
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });
        },

        async deactivate(id: number): Promise<void> {
            this.apiClient.userEndpoint
                .deactivate(id)
                .then((): any => {
                    StoreFactory.toastStore.success(
                        translate('users.deactivation.title'),
                        translate('users.deactivation.message')
                    );
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });
        },

        async login(email: string, password: string): Promise<boolean> {
            try {
                console.log('login', email);
                return true;

                //TODO: implement
                //await this.apiClient.authenticationEndpoint.login(email, password);

                await Promise.all([this.fetchCurrentUser(), this.fetchPermissions()]);

                return true;
            } catch (error) {
                StoreFactory.toastStore.danger(
                  translate('global.label.error'),
                  translate('validation.the_login_details_were_incorrect') +
                  '. ' +
                  translate('validation.please_try_again') +
                  '.'
                );
                await this.logout();
                return false;
            }
        },

        logout(): Promise<any> {
            return new Promise((resolve) => {
                this.setToken();
                this.setRefreshToken();

                StoreFactory.reset();
                router.push({ name: 'authentication.login' });

                resolve({});
            });
        },

        async resetPassword(email: string): Promise<boolean> {
            return this.apiClient.userEndpoint
              .resetPassword(email)
              .then(() => {
                  StoreFactory.toastStore.success(
                    translate('global.sent'),
                    translate('notifications.forgot_password_sent', { email: email })
                  );

                  return true;
              })
              .catch((error: any) => {
                  if (error.response.status == 404) {
                      StoreFactory.toastStore.success(
                        translate('global.sent'),
                        translate('notifications.forgot_password_sent', { email: email })
                      );

                      return true;
                  }

                  StoreFactory.toastStore.errorResponseToast(error, true);

                  return false;
              });
        },

        refreshTokenOnError(errorRequest: any, resolve: any, reject: any): void {
            this.apiClient.authenticationEndpoint
              .refresh({ user_id: user })
              .then((response: any) => {
                  const { access_token } = response.data.data;
                  axios
                    .request({
                        ...errorRequest.config,
                        headers: {
                            ...errorRequest.config.headers,
                            Authorization: `Bearer ${access_token}`,
                        },
                    })
                    .then((response: any) => {
                        resolve(response);
                    })
                    .catch((error: any) => {
                        reject(error);
                    });
              })
              .catch(async (error: any) => {
                  StoreFactory.toastStore.success(
                    translate('notifications.logged_out'),
                    translate('notifications.session_expired')
                  );

                  await this.logout();

                  reject(error);
              });
        },

        async refreshToken(user: User | undefined | null = null): Promise<unknown> {
            if (!user) {
                this.logout().then(() => {
                    StoreFactory.toastStore.success(translate('notifications.session_expired'), '');
                });
                return;
            }

            await this.apiClient.authenticationEndpoint
              .refresh({ user_id: user })
              .then((response) => {
                  this.setToken(response.data.data.access_token);
              })
              .catch((error) => {
                  StoreFactory.toastStore.errorResponseToast(error);
              });

            return;
        },

        setToken(token: string = '') {
            this.token = token;
        },

        setRefreshToken(token: string = '') {
            this.refresh_token = token;
        },

        async fetchCurrentUser(): Promise<void> {
            return this.apiClient.authenticationEndpoint.user().then((response: any) => {
                this.setCurrentUser(response.data.data);
                return response.data.data;
            });
        },

        async fetchRoles(): Promise<Role[] | []> {
            const response = await this.apiClient.authenticationEndpoint
              .roles()
              .then((response) => {
                  return {
                      ...response.data,
                      status: response.status,
                  };
              })
              .catch((error: any): any => {
                  StoreFactory.toastStore.errorResponseToast(error, true);
              });

            if (![200, 201, 204].includes(response.status)) {
                return [];
            }

            return (this.roles = response.data);
        },

        async fetchPermissions(): Promise<void> {
            const response = await this.apiClient.authenticationEndpoint
              .permissions()
              .then((response) => {
                  return {
                      ...response.data,
                      status: response.status,
                  };
              })
              .catch((error: any): any => {
                  StoreFactory.toastStore.errorResponseToast(error, true);
              });

            if (![200, 201, 204].includes(response.status)) {
                return;
            }

            this.permissions = response.data;
        },

        setPermissions(permissions: any): void {
            this.permissions = permissions;
        },

        hasPermission(permission: string, method: string): boolean {
            return this.permissions.some((p: Permission) => {
                return p.name === permission && p.method === method;
            });
        },
    },
});
