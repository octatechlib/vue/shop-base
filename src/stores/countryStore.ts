import { defineStore } from 'pinia';
import { StoreFactory } from '@/stores/index';
import type { AxiosResponse } from 'axios';
import type { Country, CountryIndexResponse } from '@/types/stores/countryStoreTypes';
import type { IndexLinksResponse, IndexMetaResponse } from '@/types/clients/Response';
import type { CountryState } from '@/types/stores/states';

export const useCountryStore = defineStore('countryStore', {
    state: (): CountryState => {
        return {
            countries: [] as Country[],
            country: {} as Country,
            meta: {} as IndexMetaResponse,
            links: {} as IndexLinksResponse,
        };
    },

    getters: {
        getCountries(state: CountryState) {
            return () => state.countries;
        },
        getCountry(state: CountryState) {
            return () => state.country;
        },
        getCountryById(state: CountryState) {
            return (id: number) =>
                state.countries.find((country: Country): boolean => {
                    return country.id === id;
                });
        },
    },

    actions: {
        async setCountry(country: Country): Promise<void> {
            this.country = country;
        },

        async searchCountries(): Promise<any> {
            const response = await this.apiClient.countryEndpoint
                .index()
                .then((response: AxiosResponse): CountryIndexResponse => {
                    return {
                        ...response.data,
                        status: response.status,
                    };
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                    return {status: error.code}
                });

            if (![200, 201, 204].includes(response.status)) {
                //todo handle error this.handleError()
                return [];
            }

            this.countries = response.data;
            this.meta = response.meta;
            this.links = response.links;

            return { items: this.countries, total: this.meta.total };
        },
    },
});
