import { defineStore } from 'pinia';
import { StoreFactory } from '@/stores/index';
import { ValidationStatusEnum } from '@/enums/validationStatusEnum';
import type { AxiosResponse } from 'axios';
import type { IndexLinksResponse, IndexMetaResponse } from '@/types/clients/Response';
import type { ValidationState } from '@/types/stores/states';
import type { AddressAutocompleteResponse, CompanyAutocompleteResponse } from '@/types/stores/validationStoreTypes';
import type { ValidateResponse } from '@/Interfaces/api/Validation';

export const useValidationStore = defineStore('validationStore', {
    state: (): ValidationState => {
        return {
            data: {},
            meta: {} as IndexMetaResponse,
            links: {} as IndexLinksResponse,
        };
    },

    getters: {
        // ...
    },

    actions: {
        validateAddress: async function (data: any): Promise<ValidateResponse> {
            const response = await this.apiClient.addressEndpoint
                .autocomplete(data)
                .then((response: AxiosResponse): AddressAutocompleteResponse => {
                    return {
                        ...response.data,
                    };
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });

            if (response) {
                const isAddressValid = response.data.length > 0;
                return {
                    data: response.data,
                    status: isAddressValid ? ValidationStatusEnum.STATUS_VALID : ValidationStatusEnum.STATUS_INVALID,
                };
            }

            return {
                data: {},
                status: ValidationStatusEnum.STATUS_INVALID,
            };
        },

        validateCompany: async function (data: any): Promise<ValidateResponse> {
            const response = await this.apiClient.companyEndpoint
                .autocomplete(data)
                .then((response: AxiosResponse): CompanyAutocompleteResponse => {
                    return {
                        ...response.data,
                    };
                })
                .catch((error: any): any => {
                    return {
                        error: error.response.data.errors,
                    };
                });

            if (response) {
                const isResponseValid = !response.errors && response.data && response.data.length > 0;
                if (isResponseValid) {
                    return {
                        data: response.data,
                        status: ValidationStatusEnum.STATUS_VALID,
                    };
                }
            }

            return {
                data: {},
                status: ValidationStatusEnum.STATUS_INVALID,
                code: response.error.status,
                title: response.error.title,
                message: response.error.detail,
            };
        },
    },
});
