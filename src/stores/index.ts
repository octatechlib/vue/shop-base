import { useToastStore } from '@/stores/toastStore';
import type { Store } from 'pinia';
import type {
    AddressStore,
    ConfigStore,
    CountryStore,
    ProductStore,
    //OrderStore,
    UserStore,
    StoreFactories,
    ToastStore,
} from '@/types/stores';

import { getActivePinia } from 'pinia';
import { ucfirst } from '@/utilities/StringUtils';

import { useAddressStore } from '@/stores/addressStore';
import { useCountryStore } from '@/stores/countryStore';
import { useProductStore } from '@/stores/productStore';
import { useUserStore } from '@/stores/userStore';
import { useConfigStore } from '@/stores/configStore';

export class StoreFactory {
    static AddressStore: AddressStore;
    static CountryStore: CountryStore;
    static ConfigStore: ConfigStore;
    static ProductStore: ProductStore;
    static UserStore: UserStore;
    static ToastStore: ToastStore;

    static initialized: boolean = false;
    static instance: StoreFactories & StoreFactory;

    public constructor() {
        if (StoreFactory.initialized) {
            return StoreFactory.instance;
        }

        this.init();

        return StoreFactory.instance;
    }

    private init(): void {
        StoreFactory.AddressStore = useAddressStore();
        StoreFactory.CountryStore = useCountryStore();
        StoreFactory.ConfigStore = useConfigStore();
        StoreFactory.ProductStore = useProductStore();
        StoreFactory.UserStore = useUserStore();
        StoreFactory.ToastStore = useToastStore();

        StoreFactory.initialized = true;

        const self = this;

        StoreFactory.instance = new Proxy(self, {
            get: function (self: StoreFactory, store: string): Store | undefined {
                const storeParameter: string = ucfirst(store);

                if (storeParameter in StoreFactory) {
                    // @ts-ignore
                    return StoreFactory[storeParameter];
                }

                return undefined;
            },
        }) as StoreFactories & StoreFactory;
    }

    static get toastStore(): ToastStore {
        if (!StoreFactory.ToastStore) {
            StoreFactory.ToastStore = useToastStore();
        }

        return StoreFactory.ToastStore;
    }

    static get configStore(): ConfigStore {
        if (!StoreFactory.ConfigStore) {
            StoreFactory.ConfigStore = useConfigStore();
        }

        return StoreFactory.ConfigStore;
    }

    static get userStore(): UserStore {
        if (!StoreFactory.UserStore) {
            StoreFactory.UserStore = useUserStore();
        }

        return StoreFactory.UserStore;
    }

    public reset(): void {
        StoreFactory.reset();
    }

    static reset(): void {
        // @ts-ignore
        getActivePinia()?._s.forEach((store: Store) => {
            if (['toastStore', 'modalStore', 'configStore'].includes(store.$id)) {
                return true;
            }

            store.$reset();
        });
    }
}
