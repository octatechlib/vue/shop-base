import { defineStore } from 'pinia';
import { StoreFactory } from '@/stores/index';
import type { AxiosResponse } from 'axios';
import type {
    AddressResource,
    AddressIndexResponse,
    AddressState,
} from '@/types/stores/addressStoreTypes';
import type {
    IndexLinksResponse,
    IndexMetaResponse
} from '@/types/clients/Response';
import { i18n } from '@/locales/i18n';
const { t: translate } = i18n;

export const useAddressStore = defineStore('addressStore', {
    state: (): AddressState => {
        return {
            addresses: [] as AddressResource[],
            address: {} as AddressResource,
            meta: {} as IndexMetaResponse,
            links: {} as IndexLinksResponse,
        };
    },

    getters: {
        getAddresses(state: AddressState): any {
            return () => state.addresses;
        },

        getAddress(state: AddressState): any {
            return () => state.address;
        },

        getAddressById(state: AddressState): any {
            // return (id: number) =>
            //     state.address.find((address: AddressResource): boolean => {
            //         //  Property 'find' does not exist on type 'AddressResource'
            //         return address.id === id;
            //     });
        },
    },

    actions: {
        async setAddress(address: AddressResource): Promise<void> {
            this.address = address;
        },

        async searchAddresses(searchData: any): Promise<any> {
            const response = await this.apiClient.addressEndpoint
                .index(searchData)
                .then((response: AxiosResponse): AddressIndexResponse => {
                    return {
                        ...response.data,
                        status: response.status,
                    };
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });

            // TODO: handle?
            //if (![200, 201, 204].includes(response.status)) {
            //return [];
            //}

            this.addresses = response.data;
            this.meta = response.meta;
            this.links = response.links;

            return { items: this.addresses, total: this.meta.total };
        },

        async search(entityId: number): Promise<any> {
            return this.apiClient.addressEndpoint
                .show(entityId)
                .then((response: AxiosResponse): AddressIndexResponse => {
                    return {
                        ...response.data.data,
                        status: response.status,
                    };
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });
        },

        async create(data: any): Promise<boolean> {
            return await this.apiClient.addressEndpoint
                .store(data)
                .then((response: AxiosResponse): boolean => {
                    StoreFactory.toastStore.success(
                        translate('global.label.new') + ' ' + translate('global.label.address'),
                        data.address.id
                    );
                    return true;
                })
                .catch((error: any): boolean => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                    return false;
                });
        },

        async update(entityId: number, data: any): Promise<void> {
            await this.apiClient.addressEndpoint
                .update(entityId, data)
                .then((): any => {
                    StoreFactory.toastStore.success(
                        translate('addresses.update.title'),
                        translate('addresses.update.message')
                    );
                })
                .catch((error: any): any => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });
        },

        async createPending(data: any): Promise<any> {
            return await this.apiClient.addressEndpoint.store(data); // no then
        },

        async updatePending(entityId: number, data: any): Promise<any> {
            return await this.apiClient.addressEndpoint.update(entityId, data); // no then
        },

        async remove(entityId: number): Promise<void> {
            await this.apiClient.addressEndpoint
                .remove(entityId)
                .then((): void => {
                    // no message for now
                })
                .catch((error: any): void => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });
        },

        async restore(entityId: number): Promise<void> {
            await this.apiClient.addressEndpoint
                .restore(entityId)
                .then((): void => {
                    // no message for now
                })
                .catch((error: any): void => {
                    StoreFactory.toastStore.errorResponseToast(error, true);
                });
        },
    },
});
