import axios from 'axios';
import { defineStore } from 'pinia';
import { i18n } from '@/locales/i18n';
import { extractAndRemoveParamsInBraces } from '@/utilities/StringUtils';

const { t: translate } = i18n;

export const useToastStore = defineStore('toastStore', {
    state: () => {
        return {
            notifications: [] as [],
        };
    },

    actions: {
        show(notification: any) {
            const defaultOptions = {
                id: Date.now() + Math.random(),
                type: 'primary',
                title: 'Notification',
                icon: 'ri-information-fill',
                message: 'This is a notification',
                timeout: 5000,
                showClose: true,
            };
            const newNotification = { ...defaultOptions, ...notification };

            // @ts-ignore
            this.notifications.push(newNotification);

            setTimeout(() => {
                /* c8 ignore next 3 */
                this.notifications = this.notifications.filter((n: any) => n.id !== newNotification.id) as [];
            }, newNotification.timeout);
        },

        success(title: string, message: string, config: any = {}) {
            this.show({
                title,
                message,
                type: 'success',
                icon: 'mdi-check-circle',
                ...config,
            });
        },

        danger(title: string, message: string, config: any = {}) {
            this.show({
                title,
                message,
                type: 'error',
                icon: 'mdi-alert-circle',
                ...config,
            });
        },

        warning(title: string, message: string, config: any = {}) {
            this.show({
                title,
                message,
                type: 'warning',
                icon: 'mdi-alert',
                ...config,
            });
        },

        removeNotification(id: any) {
            // @ts-ignore
            this.notifications = this.notifications.filter((n: any) => n.id !== id);
        },

        errorResponseToast(error: any, shouldIgnoreAuthErrors: boolean = false): void {
            if (shouldIgnoreAuthErrors && [401, 403].includes(error?.response?.status)) {
                return;
            }

            if (!error?.response?.data?.errors?.[Symbol.iterator]) {
                error.response.data.errors = [error.response.data.errors];
            }

            error?.response?.data?.errors?.forEach((error: any) => {
                const params = extractAndRemoveParamsInBraces(error.detail);
                this.danger(
                    translate(error.title),
                    translate(
                        params.cleanedString,
                        params.curlyBraces.length == 0 ? params.extractedNumbers : params.curlyBraces
                    ),
                    {
                        timeout: 15000,
                    }
                );
                return;
            });

            if (error?.response?.data?.errors) {
                return;
            }

            if (axios.isAxiosError(error)) {
                let axiosError = {
                    title: error?.response?.data?.errors[0]?.title,
                    message: error?.response?.data?.errors[0]?.detail,
                };

                if (!axiosError.message) {
                    axiosError = {
                        title: translate('global.label.error'),
                        message: error?.message,
                    };
                }

                this.danger(
                    axiosError.title ?? translate('global.label.error'),
                    axiosError.message ?? translate('global.label.error_occurred'),
                    {
                        timeout: 15000,
                    }
                );

                return;
            }

            this.danger(translate('global.label.error'), translate('global.label.error_occurred'), {
                timeout: 15000,
            });

            return;
        },
    },
});
