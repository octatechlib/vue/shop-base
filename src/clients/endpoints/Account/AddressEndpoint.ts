import type { AxiosResponse } from 'axios';
import type { ApiClientInterface } from '@/Interfaces/ApiClientInterface'
import AbstractApiEndpoint from '@/clients/AbstractApiEndpoint'

export default class AddressEndpoint extends AbstractApiEndpoint implements ApiClientInterface {
    constructor() {
        super('addresses');
    }

    async index(params: any = {}): Promise<AxiosResponse> {
        const request: any = {};
        request.params = params;

        return await this.client.get('/', request);
    }

    show(id: number) {
        return this.client.get(`/${id}`);
    }

    create(data: any) {
        return this.client.post(`/`, data);
    }

    update(id: number, data: any) {
        return this.client.put(`/${id}`, data);
    }

    remove(id: number) {
        return this.client.patch(`/${id}/remove`);
    }

    restore(id: number) {
        return this.client.patch(`/${id}/restore`);
    }

    autocomplete(data: any) {
        const queryParams = new URLSearchParams(data);
        const paramsString = queryParams.toString();

        return this.client.get(`/autocomplete?${paramsString}`); // this.client.get(`/autocomplete`, data);
    }

    destroy(id: number) {
        // TODO: remove when PayrollApi does not require
        return new Promise((resolve, reject) => {
            reject(`Unable to permanently destroy: ${id}`);
        });
    }
}
