import type { AxiosResponse } from 'axios';
import AbstractApiEndpoint from '@/clients/AbstractApiEndpoint'

export default class UserEndpoint extends AbstractApiEndpoint {
    constructor() {
        super('users');
    }

    async resetPassword(email: string): Promise<any> {
        return await this.client.post('reset-password', {
            email,
        });
    }

    async resetPasswordUpdate(
        email: string,
        password: string,
        password_confirmation: string,
        token: string
    ): Promise<any> {
        return await this.client.patch(`reset-password/${token}`, {
            email,
            password,
            password_confirmation,
        });
    }

    async index(params: any = {}): Promise<AxiosResponse> {
        const request: any = {};
        request.params = params;

        return this.client.get('/', request);
    }

    create(data: any): Promise<any> {
        return this.client.post('/', data);
    }

    async show(id: number): Promise<any> {
        return this.client.get(`/${id}`);
    }

    async update(id: any, data: any): Promise<any> {
        return this.client.put(`/${id}`, data);
    }

    async delete(id: any): Promise<any> {
        return this.client.delete(`/${id}`);
    }

    async activate(id: number): Promise<any> {
        return this.client.post(`/${id}/activate`);
    }

    async deactivate(id: number): Promise<any> {
        return this.client.post(`/${id}/deactivate`);
    }
}
