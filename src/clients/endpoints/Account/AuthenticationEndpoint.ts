
import type { AxiosResponse } from 'axios';
import { StoreFactory } from '@/stores';
import type { LoginResponse, RefreshResponse } from '@/Interfaces/api/Authentication';
import type { RefreshToken } from '@/types/clients/requests';
import AbstractApiEndpoint from '@/clients/AbstractApiEndpoint'
import type { User } from '@/types/stores/userStoreTypes'

export default class AuthenticationEndpoint extends AbstractApiEndpoint {
    constructor() {
        super('auth');
    }

    login(email: string, password: string) {
        return new Promise((resolve, reject) => {
            this.client
                .post<LoginResponse>('login', {
                    email,
                    password,
                })
                .then((response) => {
                    const { access_token, refresh_token } = response.data.data;
                    StoreFactory.userStore.setToken(access_token);
                    StoreFactory.userStore.setRefreshToken(refresh_token);
                    resolve(response);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    async user() {
        return this.client.get('user');
    }

    async logout() {
        return this.client.delete('logout');
    }

    refresh(parameters: RefreshToken | User = {}): Promise<AxiosResponse> {
        return this.client.put<RefreshResponse>(`refresh/${StoreFactory.userStore.getRefreshToken}`, parameters);
    }

    async permission() {
        return this.client.get('permission');
    }

    async permissions(): Promise<AxiosResponse> {
        return this.client.get('/permissions');
    }

    async valid() {
        return this.client.get('valid');
    }

    async clients() {
        return this.client.get('clients');
    }

    async roles() {
        return this.client.get('roles');
    }
}
