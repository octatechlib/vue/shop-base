import type { AxiosResponse } from 'axios';
import type { ApiClientInterface } from '@/Interfaces/ApiClientInterface'
import AbstractApiEndpoint from '@/clients/AbstractApiEndpoint'

export default class ProductEndpoint extends AbstractApiEndpoint implements ApiClientInterface {
  constructor() {
    super('countries');
  }

  async index(params: any = {}): Promise<AxiosResponse> {
    const request: any = {};
    request.params = params;

    return await this.client.get('/', request);
  }

  show(id: number) {
    return this.client.get(`/${id}`);
  }

  create(data: any) {
    return new Promise((resolve, reject) => {
      reject(`Unable to store`);
    });
  }

  update(id: number, data: any) {
    return new Promise((resolve, reject) => {
      reject(`Unable to update: ${id}`);
    });
  }

  destroy(id: number) {
    return new Promise((resolve, reject) => {
      reject(`Unable to permanently destroy: ${id}`);
    });
  }
}
