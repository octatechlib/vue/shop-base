import axios, { type AxiosInstance } from 'axios';
import { StoreFactory } from '@/stores';

export default class AbstractApiEndpoint {
    client!: AxiosInstance;
    baseURL: string = import.meta.env.VITE_API_URL_AUTH;
    prefix!: string;

    constructor(prefix: string) {
        this.prefix = prefix;
        this.setClient(this.baseURL);
    }

    setClient(baseURL: string) {
        this.client = axios.create({
            baseURL: baseURL + this.prefix + '/',
        });

        this.client.interceptors.request.use(this.authMiddleware.bind(this));
        this.client.interceptors.response.use(this.handleResponse.bind(this), this.handleErrorResponse.bind(this));
    }

    authMiddleware(config: any) {
        if (StoreFactory.userStore.getToken) {
            config.headers.Authorization = `Bearer ${StoreFactory.userStore.getToken}`;
        }

        return config;
    }

    handleResponse(response: any) {
        return Promise.resolve(response);
    }

    handleErrorResponse(errorRequest: any) {
        return new Promise((resolve, reject) => {
            const errorResponseStatusCodes = [401, 403];

            if (!errorResponseStatusCodes.includes(errorRequest?.response?.status)) {
                reject(errorRequest);
                return;
            }

            if (
                !errorRequest?.request?.responseURL?.includes('/auth/refresh') &&
                !errorRequest?.request?.responseURL?.includes('/auth/login')
            ) {
                StoreFactory.userStore.refreshTokenOnError(errorRequest, resolve, reject);
                return;
            }

            reject(errorRequest);
        });
    }
}
