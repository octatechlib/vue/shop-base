import AuthenticationEndpoint from '@/clients/endpoints/Account/AuthenticationEndpoint';
import AddressEndpoint from '@/clients/endpoints/Account/AddressEndpoint'
import CategoryEndpoint from '@/clients/endpoints/Account/CategoryEndpoint'
import CountryEndpoint from '@/clients/endpoints/Account/CountryEndpoint'
import ProductEndpoint from './endpoints/Account/ProductEndpoint';
import OrderEndpoint from '@/clients/endpoints/Account/OrderEndpoint'
import UserEndpoint from '@/clients/endpoints/Account/UserEndpoint';

export default class ApiClient {
    authenticationEndpoint!: AuthenticationEndpoint;
    addressEndpoint!: AddressEndpoint;
    categoryEndpoint!: CategoryEndpoint;
    countryEndpoint!: CountryEndpoint;
    productEndpoint!: ProductEndpoint;
    orderEndpoint!: OrderEndpoint;
    userEndpoint!: UserEndpoint;

    constructor() {
        this.initializeClasses();
    }

    initializeClasses() {
        this.authenticationEndpoint = new AuthenticationEndpoint();
        this.addressEndpoint = new AddressEndpoint();
        this.categoryEndpoint = new CategoryEndpoint();
        this.countryEndpoint = new CountryEndpoint();
        this.productEndpoint = new ProductEndpoint();
        this.orderEndpoint = new OrderEndpoint();
        this.userEndpoint = new UserEndpoint();
    }
}
