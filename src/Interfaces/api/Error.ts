export interface ErrorResponse {
    status: any;
    title?: string;
    detail?: string;
}
