export interface LoginResponse {
    data: {
        access_token: string;
        refresh_token: string;
    };
}

export interface RefreshResponse {
    data: {
        access_token: string;
    };
}
