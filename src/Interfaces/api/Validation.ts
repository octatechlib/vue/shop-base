import type { ValidationStatusEnum } from '@/enums/validationStatusEnum';

export interface ValidateResponse {
    data: object;
    status: ValidationStatusEnum;
    title?: string | undefined;
    message?: string | undefined;
    code?: number | null | undefined;
    hasApi?: boolean | undefined;
}
