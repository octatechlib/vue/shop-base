export interface ApiResourceInterface {
    index: () => Promise<any>;
    show: (id: number, ...args: any) => Promise<any>;
    store: (data: any, ...args: any) => Promise<any>;
    update: (id: number, data: any, ...args: any) => Promise<any>;
    destroy: (id: number, ...args: any) => Promise<any>;
}
