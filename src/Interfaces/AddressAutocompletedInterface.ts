export interface AddressAutocompletedInterface {
    iso: string | undefined;
    province: string | undefined;
    city: string | undefined;
    postal_code: string | undefined;
    street: string | undefined;
    street_number: string | undefined;
    extension: string | undefined;
}
