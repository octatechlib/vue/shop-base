export interface StoredEmail {
    id?: number | null | undefined;
    email: string | null;
    type: string | null;
}


export interface FormEmail {
    id?: number | null | undefined;
    type: string | undefined;
    email: string | undefined;
}
