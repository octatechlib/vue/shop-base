import type { ApiResourceInterface } from '@/Interfaces/ApiResourceInterface';

export interface ApiClientInterface extends ApiResourceInterface {
    // restore?: (id: number, ...args: any) => Promise<any>;
}
