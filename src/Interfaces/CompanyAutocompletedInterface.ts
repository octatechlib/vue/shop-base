export interface CompanyAutocompletedInterface {
    name: string | undefined;
    trade_number: string | undefined;
    trade_type: string | undefined;
    partnership_number: string | undefined;
    registered_at: string | undefined;
    approved_at: string | undefined;
    company_size: number | undefined;
    addresses: CompanyAddressAutocompleted[] | undefined;
}

export interface CompanyAddressAutocompleted {
    type: string | undefined;
    street: string | undefined;
    street_number: number | string | undefined;
    street_number_extension: string | undefined;
    post_code: string | undefined;
    city: string | undefined;
    country_id: number | undefined;
    country_name: string | undefined;
    country_iso: string | undefined;
    is_protected: boolean | undefined;
}
