export interface Company {
    id: string;
    name: string;
    vat_type: string | null;
    national_trade_id: string;
    vat_number: string;
    vatTypesDropdown: [];
    company_branch: {
        id: string;
        company_id: string;
        branch: string;
    }[];
}
