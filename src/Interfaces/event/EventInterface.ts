export interface EventInterface {
    register: () => void;
    mount(data?: { value: any }, props?: { type: string | undefined }): void;
}
