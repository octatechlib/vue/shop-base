// Styles
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/styles';
import i18n from '/src/locales/i18n.ts';
import { createVueI18nAdapter } from 'vuetify/locale/adapters/vue-i18n';
import { useI18n } from 'vue-i18n';

// Composables
import { createVuetify } from 'vuetify';
import { aliases, mdi } from 'vuetify/iconsets/mdi';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides

const octaLight = {
    dark: false,
    colors: {
        'background': '#FFFFFF',
        'primary': '#dd0000',
        'secondary': '#000E4A',
        'error': '#FB3640',
        'info': '#2196F3',
        'success': '#4CAF50',
        'warning': '#FFC107',
        'lightblue': '#14c6FF',
        'yellow': '#FFCF00',
        'pink': '#FF1976',
        'orange': '#FF8657',
        'magenta': '#C33AFC',
        'darkblue': '#1E2D56',
        'gray': '#909090',
        'gray-light': '#F9F9F9',
        'neutral-gray': '#9BA6C1',
        'green': '#2ED47A',
        'red': '#FF5c4E',
        'dark-blues-hade': '#308DC2',
        'lightgray': '#BDBDBD',
        'lightpink': '#FFCFE3',
        'white': '#FFFFFF',
    },
    variables: {
        'theme-on-primary': '#000E4A',
        'button-colored-disabled': false,
    },
};

const octaDark = {
    dark: true,
    colors: {
        'background': '#FFFFFF',
        'primary': '#dd0000',
        'secondary': '#000E4A',
        'error': '#FB3640',
        'gray-light': '#F9F9F9',
    },
    variables: {
        'theme-on-primary': '#000E4A',
        'button-colored-disabled': false,
    },
};

export const vuetify = createVuetify({
    theme: {
        defaultTheme: 'octaLight',
        themes: {
            octaLight,
            octaDark,
        },
    },

    icons: {
        defaultSet: 'mdi', //'mdiSvg', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
        aliases,
        sets: {
            mdi,
        },
    },
    defaults: {
        global: {
            ripple: false, // Remove strange Vuetify click effect on items
        },
        VContainer: {
            class: 'v-container-class',
        },
        VBtn: {
            class: 'v-btn-class',
        },
        VField: {
            class: 'v-field-class',
        },
        VDataTableServer: {
            class: 'tw-border tw-border-gray-border !tw-rounded-md',
        },
        VTextField: {
            class: 'v-text-field-class',
            variant: 'outlined',
            persistentPlaceholder: true,
            color: 'secondary',
            flat: true,
            density: 'compact',
            hideDetails: 'auto',

            VLabel: {
                class: 'v-label-class',
            },
        },
        VInput: {
            class: 'v-input-class mb-5', //
            VLabel: {
            },
        },
        VSelect: {
            class: 'v-select-class',
            variant: 'outlined',
            persistentPlaceholder: true,
            flat: true,
            density: 'compact',
            hideDetails: 'auto',
        },
        VCard: {
            class: 'v-card-class bg-white rounded-lg border-0',
            variant: 'outlined',

            VCardTitle: {
                class: 'v-card-title-class',
            },

            VCardSubtitle: {
                class: 'v-card-subtitle-class',
            },
        },
        VTable: {
            VDataTable: {
                class: '!tw-bg-primary',
            },
        },
        VList: {
            class: 'v-list-class',
            VListSubheader: {
                class: 'v-list-subheader-class',
                style: 'padding-inline-start: 10px !important',
            },
        },
        VCheckbox: {
            class: 'v-list-checkbox-class',
            hideDetails: 'auto',
            density: 'compact',
            baseColor: '#e6e6e6',
            VCheckboxBtn: {
                class: 'tw-gap-2',
            },
        },

        VNavigationDrawer: {
            class: 'v-navigation-drawer',
        },
    },
    locale: {
        adapter: createVueI18nAdapter({ i18n, useI18n }),
        locale: i18n.global.locale.value ?? 'en_gb',
        fallback: 'en_gb',
    },
    components,
    directives,
});

export default vuetify;
