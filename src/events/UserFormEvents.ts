import type { EventInterface } from '@/Interfaces/event/EventInterface';
import type { TypesEnum as UserTypes, ValueOf } from '@/enums/userEnum';

export class UserFormEvents implements EventInterface {
    register() {
        // ...
    }

    mount(data?: { value: any }, props?: { type?: string | undefined }) {
        if (data && props && typeof props.type !== 'undefined' && props.type.length) {
            data.value.type = props.type;
        }
    }

    showExtraFields(params: { fromData: { type: ValueOf<typeof UserTypes> } }) {
        // no events for now, later hide and show extra fields on type change
        console.info('Show extra fields for currently selected type: ' + params.fromData.type);
    }
}
