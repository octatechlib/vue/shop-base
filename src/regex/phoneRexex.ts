/// requires +[1-4] country code
export const GLOBAL_PHONE_REGEX: RegExp = /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/im;
