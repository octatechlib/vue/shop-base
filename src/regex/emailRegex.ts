export const EMAIL_REGEX: RegExp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/u;
export const ALPHA_REGEX: RegExp = /^[\p{L}\p{M}]+$/u;
export const ALPHA_NUM_REGEX: RegExp = /^[\p{L}\p{M}\p{N}]+$/u;
export const ALPHA_DASH_REGEX: RegExp = /^[\p{L}\p{M}\p{N}\p{Pd}]+$/u;
