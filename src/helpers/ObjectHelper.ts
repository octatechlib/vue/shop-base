import { checkRoutePermission } from './router';

export class ObjectHelper {
    /**
     * Filter out empty objects, useful before sending data to the api's
     *
     * @param {} data Array or object to filter
     * @param shouldConvertToArray
     */
    filterEmpty(data: { [key: string]: any }, shouldConvertToArray: boolean = false): Object {
        const filteredData: any = {};

        Object.keys(data)
            .filter((key: number | string) => !!data[key])
            .forEach((param: string | number): void => {
                filteredData[param] = shouldConvertToArray ? [data[param]] : data[param];
            });

        return filteredData;
    }

    /**
     * Return only items that have the value isAllowed set to true
     *
     * @param {} data Array or object to filter
     * @param {[] | {}} returnAs return as an array or object
     */
    filterAllowed(data: { [key: string]: any }, returnAs: {} | [] = {}): [] | {} {
        return Object.keys(data)
            .filter((key: any) => data[key].isAllowed)
            .reduce((filtered: { [key: string]: any }, key: string) => {
                filtered[key] = data[key];
                return filtered;
            }, returnAs);
    }

    /**
     * @param {} data Array or object to filter
     * @param { [] } keys keys you want to extract with their value
     * @param { [] | {} } returnAs return as an array or object
     */
    filterByKeys(data: { [key: string]: any }, keys: string[], returnAs: {} | [] = {}): { [key: string]: any } {
        return keys.reduce((result: { [key: string]: any }, item: string) => {
            result[item] = data[item];
            return result;
        }, returnAs);
    }

    getValues(data: [] | {}, defaultValue: any = null): any {
        const hasData: boolean = Array.isArray(data) ? data.length > 0 : Object.keys(data).length > 0;

        return hasData ? data : defaultValue;
    }

    getPermission(route: any, router: any) {
        return checkRoutePermission(route, router);
    }
}
