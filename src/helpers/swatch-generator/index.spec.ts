import { describe, it, expect } from 'vitest';
import { useTailwindColors } from './index';

describe('Swatch', () => {
    it('generate swatch colors', () => {
        const color = '#8e44ad';
        const swatch = useTailwindColors(color);
        expect(swatch).toHaveProperty('color50');
        expect(swatch).toHaveProperty('color100');
        expect(swatch).toHaveProperty('color200');
        expect(swatch).toHaveProperty('color300');
        expect(swatch).toHaveProperty('color400');
        expect(swatch).toHaveProperty('color500');
        expect(swatch).toHaveProperty('color600');
        expect(swatch).toHaveProperty('color700');
        expect(swatch).toHaveProperty('color800');
        expect(swatch).toHaveProperty('color900');
    });
});
