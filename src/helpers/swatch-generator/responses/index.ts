export function output(palettes: any) {
    const shaped = {};

    palettes.forEach((palette: any) => {
        const swatches = {};
        palette.swatches
            .filter((swatch: any) => ![0, 1000].includes(swatch.stop))
            .forEach((swatch: any) => Object.assign(swatches, { [swatch.stop]: swatch.hex.toUpperCase() }));

        Object.assign(shaped, { [palette.name]: swatches });
    });

    return shaped;
}
