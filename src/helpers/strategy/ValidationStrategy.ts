import type { AddressValidationResponseType } from '@/types/stores/validationStoreTypes';
import type { ValidationStore } from '@/types/stores';
import type { AddressPayloadInterface } from '@/helpers/AutocompleteCall';
import { ValidationStatusEnum } from '@/enums/validationStatusEnum';
import { AutocompleteCall } from '@/helpers/AutocompleteCall';
import { storeFactory } from '@/main';
import { i18n } from '@/locales/i18n';

const { t: translate } = i18n;

abstract class ValidationStrategy<T extends object = any> {
    status: ValidationStatusEnum;

    constructor(public validationStore: ValidationStore, public form: any, public payload: object) {
        this.status = ValidationStatusEnum.STATUS_INVALID;
    }

    abstract validate(): Promise<any>;
}

class ValidateWithApiStrategy extends ValidationStrategy<object> {
    validate(): Promise<any> {
        const c = new AutocompleteCall(storeFactory.validationStore, this.payload);
        return c.autocompleteAddress();
    }
}

class ValidateWithRulesStrategy extends ValidationStrategy<object> implements ValidationStrategyInterface {
    async validate(): Promise<AddressValidationResponseType | any> {
        return new Promise(async (resolve, reject) => {
            await this.form.value
                .validate()
                .then((response: any) => {
                    if (response.valid) {
                        resolve({
                            data: {},
                            status: ValidationStatusEnum.STATUS_VALID,
                            message: translate('validation.statuses.validated'),
                            hasApi: false,
                        });
                    }

                    resolve({
                        data: {},
                        status: ValidationStatusEnum.STATUS_INVALID,
                        message:
                            translate('validation.statuses.invalid') +
                            '. ' +
                            translate('validation.missing_required_fields') +
                            '.',
                        hasApi: false,
                    });
                })
                .catch((error: any) => {
                    console.error(error);

                    reject({
                        data: {},
                        status: ValidationStatusEnum.STATUS_INVALID,
                        message:
                            translate('validation.statuses.invalid') +
                            '. ' +
                            translate('global.label.error_occurred') +
                            '.',
                        hasApi: false,
                    });
                });
        });
    }
}

export class ValidateAddress {
    isSkippingApi: boolean = false;

    constructor(public validationStore: ValidationStore, public form: any, public payload: AddressPayloadInterface) {}

    hydrate(): void {
        this.payload = Object.entries(this.payload)
            .filter(([_, v]) => v != null && v != '')
            .reduce((acc, [k, v]) => ({ ...acc, [k]: v }), {});
    }

    run(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.hydrate();
            if (this.form && this.hasMissingFields()) {
                this.form.value.validate();

                reject({
                    data: {},
                    status: ValidationStatusEnum.STATUS_INVALID,
                    message: translate('validation.missing_required_fields'),
                });
            }

            resolve({}); // fields ok, continue
        }).then(() => {
            const strategyClassExists = this.payload?.iso && this.payload.iso in REQUESTING_ISO_CODES;

            const strategy: ValidationStrategyInterface =
                strategyClassExists && !this.isSkippingApi
                    ? new ValidateWithApiStrategy(this.validationStore, this.form, this.payload)
                    : new ValidateWithRulesStrategy(this.validationStore, this.form, this.payload);

            if (this.isSkippingApi) {
                console.warn('Address validation is skipping the API call. Check config settings.');
            }

            return strategy.validate();
        }); // no catch
    }

    setApiCallStatus(isSkippingApi: boolean | undefined = false) {
        this.isSkippingApi = isSkippingApi;
        return this;
    }

    private hasMissingFields() {
        return !this.payload.iso || !this.payload.postal_code || !this.payload.street_number;
    }
}

interface ValidationStrategyInterface {
    validate(): Promise<any>;
}

export const REQUESTING_ISO_CODES = {
    nl: ValidateWithApiStrategy,
};

export function hasApiValidation(iso: any) {
    return iso in REQUESTING_ISO_CODES
}
