import type { Component } from 'vue';

type RouterApplication = {
    name: string;
    pathPrefix: string;
    layout: string;
    routes: Array<RouterRoute>;
};

type RouterRoute = {
    path: string;
    name: string;
    component: Component;
};

export const routeMapper = (route: RouterRoute, app: RouterApplication) => {
    return {
        ...route,
        name: `${app.name}.${route.name}`,
    };
};

export const applicationMapper = (app: RouterApplication) => {
    return {
        path: `${app.pathPrefix}`,
        name: app.name,
        component: () => import(`../../layouts/${app.layout}.vue`),
        children: app.routes.map((route: any) => {
            return routeMapper(route, app);
        }),
    };
};

export const checkRoutePermission = (route: any, router: any) => {
    return router.resolve(route).meta.permission();
};

export default {
    applicationMapper,
    checkRoutePermission,
};
