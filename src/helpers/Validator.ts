import { ALPHA_DASH_REGEX, ALPHA_NUM_REGEX, ALPHA_REGEX, EMAIL_REGEX } from './regex/emailRegex';
import { POS_INTEGER_REGEX, DECIMAL_INT_REGEX } from './regex/numberRegex';
import { i18n } from '@/main';
import { GLOBAL_PHONE_REGEX } from './regex/phoneRexex';

const { t: translate } = i18n.global;

export class Validator {
    constructor(private _rules: string = '') {}

    public validate(): any[] {
        const toValidate: any[] = [];

        this._rules.split('|')?.forEach((rule: string) => {
            const ruleSet: string[] = rule.split(':');
            const parameters: string[] = ruleSet[1]?.split(',') ?? [];

            toValidate.push(Validator.formatRule(ruleSet[0], parameters));
        });

        return toValidate;
    }

    public static formatRule(rule: string, parameters: any[]) {
        return {
            min: (value: string) =>
                (value && value.length >= parameters[0]) ||
                translate('validation.under_req_length', { length: parameters[0] }),
            max: (value: any) => {
                if (value !== null && value !== undefined) {
                    const stringValue = typeof value === 'number' ? value.toString() : value;
                    return (
                        stringValue.length <= parameters[0] ||
                        translate('validation.over_req_length', { length: parameters[0] })
                    );
                }
                return true;
            },
            required: (value: string) => !!value || translate('validation.is_required'),
            isNotUndefined: (value: string) => !!value || translate('validation.should_not_be_undefined'),
            email: (value: string) => EMAIL_REGEX.test(value) || translate('validation.invalid_email'),
            phone_number: (value: string) => GLOBAL_PHONE_REGEX.test(value) || translate('validation.invalid_phone'),
            alpha: (value: string) => ALPHA_REGEX.test(value) || translate('validation.should_be_alpha'),
            alpha_num: (value: string) => ALPHA_NUM_REGEX.test(value) || translate('validation.should_be_alpha_num'),
            alpha_dash: (value: string) => ALPHA_DASH_REGEX.test(value) || translate('validation.should_be_alpha_dash'),
            pos_int: (value: string) => POS_INTEGER_REGEX.test(value) || translate('validation.pos_integer'),
            decimal_int: (value: string) =>
                DECIMAL_INT_REGEX.test(value) || translate('validation.should_be_integer_or_decimal'),
            greaterThan: (value: string) =>
                (value && value > parameters[0]) ||
                translate('validation.should_be_greater_than', { value: parameters[0] }),
            greaterThanOrEqual: (value: string) =>
                (value && value >= parameters[0]) ||
                translate('validation.should_be_greater_than_equal', { value: parameters[0] }),
            smallerThan: (value: string) =>
                (value && value < parameters[0]) ||
                translate('validation.should_be_smaller_than', { value: parameters[0] }),
            smallerThanOrEqual: (value: string) =>
                (value && parseFloat(value) <= parameters[0]) ||
                translate('validation.should_be_smaller_than_equal', { value: parameters[0] }),
            same: (value: string) => value === parameters[0] || translate('validation.required'),
            dateAfter: (value: string) =>
                (value && new Date(value) > Validator.createDate(parameters[0])) ||
                translate('validation.date_should_be_after', { value: parameters[0] }),
            dateAfterOrEqual: (value: string) =>
                (value && new Date(value) >= Validator.createDate(parameters[0])) ||
                translate('validation.date_should_be_after_equal', { value: parameters[0] }),
            dateBefore: (value: string) =>
                (value && new Date(value) < Validator.createDate(parameters[0])) ||
                translate('validation.date_should_be_before', { value: parameters[0] }),
            dateBeforeOrEqual: (value: string) =>
                (value && new Date(value) <= Validator.createDate(parameters[0])) ||
                translate('validation.date_should_be_before_or_equal', { value: parameters[0] }),
            in: (value: string) =>
                (!!value && parameters.includes(value)) ||
                translate('validation.should_be_in_array', { value: parameters.join(', ') }),
            valid: () => parameters[0] === 'true' || translate('validation.invalid_value'),
            string: (value: any) => typeof value === 'string' || translate('validation.should_be_a_string'),
            numeric: (value: any) => !isNaN(Number(value)) || translate('validation.should_be_a_number'),
            integer: (value: any) => Number.isInteger(parseInt(value)) || translate('validation.should_be_an_integer'),
            regex: (value: any) =>
                (value && new RegExp(parameters[0]).test(value)) || translate('validation.should_match_a_pattern'),
            regexExact: (value: any) =>
                (value && new RegExp(parameters[0]).test(value)) ||
                translate('validation.should_match_exact_pattern', { pattern: parameters[0] }),
        }[rule];
    }

    set rules(rules: string) {
        this._rules = rules;
    }

    public setRules(rules: string): Validator {
        this._rules = rules;

        return this;
    }

    public static createDate(date: string): Date {
        return date === 'now' ? new Date() : new Date(date);
    }
}