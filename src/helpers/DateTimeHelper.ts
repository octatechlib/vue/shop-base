export class DateTimeHelper {

    createDateTimeForLocale(dateTime: Date | string | null | any, locale: string = 'en-GB'): string | null {
        if (!dateTime) {
            return null;
        }

        const isoDateTime: Date = new Date(dateTime);

        return isoDateTime
            ? isoDateTime.toLocaleDateString(locale) + ' ' + isoDateTime.toLocaleTimeString(locale)
            : null;
    }

    formatDate(dateInput: Date | string | null | any): string | null {
        const date = new Date(dateInput);
        return `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(2, '0')}-${String(date.getDate()).padStart(
            2,
            '0'
        )}`;
    }

    // getToday(): string {
    //     return this.formatDate(new Date().toString()).toString();
    // }

    dayMonthYearFormat(dateInput: Date | string | null | any): string | null {
        const date = new Date(dateInput);
        return `${String(date.getDate()).padStart(2, '0')}-${String(date.getMonth() + 1).padStart(
            2,
            '0'
        )}-${date.getFullYear()}`;
    }
}
