// @ts-ignore
import nl from './pl.json';
// @ts-ignore
import en from './en.json';

export default {
    nl,
    en,
};
