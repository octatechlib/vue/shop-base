import { createI18n } from 'vue-i18n';
import messages from '@/locales';
import { useStorage } from '@vueuse/core';

const i18nInstance = createI18n({
    legacy: false,
    globalInjection: true,
    locale: useStorage('octa-shop/config/locale', 'en').value, // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // set locale messages
    modifiers: {
        snakeCase: (str) => (<string>str).split(' ').join('_'),
    },
    // If you need to specify other options, you can set other options
    // ...
});

export default i18nInstance;

export const i18n = i18nInstance.global;
