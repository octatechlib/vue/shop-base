import InvoicesIndexView from '@/views/Owner/Invoices/IndexPage.vue';
import InvoicesNewView from '@/views/Owner/Invoices/IndexPage.vue'; // TODO: replace
//import { InvoiceGuard } from '@/guards/InvoiceGuard';

//const guard = new InvoiceGuard();

export default [
  {
    path: '/invoices',
    name: 'index',
    component: InvoicesIndexView,
    //beforeEnter: () => guard.canView(),
  },
  {
    path: '/invoices/new',
    name: 'new',
    component: InvoicesNewView,
    //beforeEnter: () => guard.canView(),
  },
];
