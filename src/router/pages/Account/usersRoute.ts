import UsersIndexView from '@/views/Owner/Users/IndexPage.vue';
import UserShowView from '@/views/Owner/Users/ShowPage.vue';
import UserCreateView from '@/views/Owner/Users/CreatePage.vue';
import UserEditView from '@/views/Owner/Users/EditPage.vue';
import { UserGuard } from '@/guards/UserGuard';

const guard = new UserGuard();

export default [
    {
        path: '/users',
        name: 'index',
        component: UsersIndexView,
        beforeEnter: () => guard.canView(),
    },
    {
        path: '/users/:id',
        name: 'show',
        component: UserShowView,
        beforeEnter: () => guard.canShow(),
    },
    {
        path: '/users/new',
        name: 'create',
        component: UserCreateView,
        props: (route: { query: HTMLObjectElement }) => ({
            type: route.query.type,
        }),
        beforeEnter: () => guard.canCreate(),
        meta: {
            permission: (): boolean => guard.canCreate(),
        },
    },
    {
        path: '/users/:id/edit',
        name: 'edit',
        component: UserEditView,
        props: true,
        beforeEnter: () => guard.canUpdate(),
    },
    {
        path: '/users/active',
        name: 'active',
        component: UsersIndexView, // TODO: change
        props: true,
        beforeEnter: () => guard.canUpdate(),
    },
    {
        path: '/users/inactive',
        name: 'inactive',
        component: UsersIndexView, // TODO: change
        props: true,
        beforeEnter: () => guard.canUpdate(),
    },
];
