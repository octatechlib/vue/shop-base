import CategoriesIndexView from '@/views/Owner/Categories/IndexPage.vue';
//import { CategoryGuard } from '@/guards/CategoryGuard';

//const guard = new CategoryGuard();

export default [
  {
    path: '/categories',
    name: 'index',
    component: CategoriesIndexView,
    //beforeEnter: () => guard.canView(),
  },
];
