import CalendarIndexView from '@/views/Owner/Calendar/IndexPage.vue';
//import { CalendarGuard } from '@/guards/UserGuard';

//const guard = new CalendarGuard();

export default [
  {
    path: '/calendar',
    name: 'index',
    component: CalendarIndexView,
    //beforeEnter: () => guard.canView(),
  },
];
