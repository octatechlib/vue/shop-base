import Login from '@/views/Guest/LoginView.vue';
import ForgotPassword from '@/views/Guest/ForgotPasswordView.vue';
import UserResetPassword from '@/views/Guest/ResetPassword.vue';

export default [
    {
        path: 'login',
        name: 'login',
        component: Login,
    },
    {
        path: 'forgot-password',
        name: 'forgot-password',
        component: ForgotPassword,
    },
    {
        path: 'activate/:token',
        name: 'activate',
        component: UserResetPassword,
    },
    {
        path: 'reset-password/:token',
        name: 'reset-password',
        component: UserResetPassword,
    },
];
