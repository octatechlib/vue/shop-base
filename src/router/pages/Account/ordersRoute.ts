import OrdersIndexView from '@/views/Owner/Orders/IndexPage.vue';
import OrdersPendingView from '@/views/Owner/Orders/IndexPage.vue'; // TODO: replace
//import { OrderGuard } from '@/guards/OrderGuard';

//const guard = new OrderGuard();

export default [
  {
    path: '/orders',
    name: 'index',
    component: OrdersIndexView,
    //beforeEnter: () => guard.canView(),
  },
  {
    path: '/orders/pending',
    name: 'pending',
    component: OrdersPendingView,
    //beforeEnter: () => guard.canView(),
  },
];
