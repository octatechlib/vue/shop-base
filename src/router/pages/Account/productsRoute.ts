import { storeFactory } from '@/main';

import ProductsIndexView from '@/views/Owner/Products/IndexPage.vue';
import ProductsCreateView from '@/views/Owner/Products/CreatePage.vue';
import ProductsEditView from '@/views/Owner/Products/EditPage.vue';
import ProductsShowView from '@/views/Owner/Products/ShowPage.vue';
import ProductsInStockView from '@/views/Owner/Products/IndexPage.vue';// TODO: replace
import ProductsOutOfStockView from '@/views/Owner/Products/IndexPage.vue';// TODO: replace
import ProductsDiscountsView from '@/views/Owner/Products/IndexPage.vue';// TODO: replace
import { ProductsGuard } from '@/guards/ProductsGuard';

const guard = new ProductsGuard();

export default [
  {
    path: '/products',
    name: 'index',
    component: ProductsIndexView,
    beforeEnter: () => guard.canView(),
  },
  {
    path: '/products/new',
    name: 'create',
    component: ProductsCreateView,
    beforeEnter: () => guard.canView(),
  },
  {
    path: '/products/:id',
    name: 'show',
    component: ProductsShowView,
    beforeEnter: () => guard.canView(),
  },
  {
    path: '/products/:id/edit',
    name: 'update',
    component: ProductsEditView,
    beforeEnter: () => guard.canUpdate(),
    // beforeEnter: (to: any) =>
    //   storeFactory.productStore
    //     .isActivated(to.params.id)
    //     .then((isProductActive: boolean) => isProductActive && guard.canUpdate()),
  },
  {
    path: '/products/in-stock',
    name: 'in-stock',
    component: ProductsInStockView,
    beforeEnter: () => guard.canView(),
  },
  {
    path: '/products/out-of-stock',
    name: 'out-of-stock',
    component: ProductsOutOfStockView,
    beforeEnter: () => guard.canSeeOutOfStock(),
  },
  {
    path: '/products/discounts',
    name: 'discounts',
    component: ProductsDiscountsView,
    beforeEnter: () => guard.canSeeDiscounts(),
  },
];
