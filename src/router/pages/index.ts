import { applicationMapper } from '@/helpers/router';

import authentication from './Account/authenticationRoute';
import dashboard from './Account/dashboardRoute';
import usersRoute from './Account/usersRoute';
import calendarRoute from './Account/calendarRoute';
import productsRoute from './Account/productsRoute';
import categoriesRoute from './Account/categoriesRoute';
import ordersRoute from './Account/ordersRoute';
import invoicesRoute from './Account/invoicesRoute';

const routes = [
    {
        pathPrefix: '/auth',
        name: 'authentication',
        routes: authentication,
        layout: 'GuestLayout',
    },
    {
        pathPrefix: '',
        name: 'dashboard',
        routes: dashboard,
        layout: 'AdministrationLayout',
    },
    {
        pathPrefix: '/products',
        name: 'products',
        routes: productsRoute,
        layout: 'AdministrationLayout',
    },
    {
        pathPrefix: '/categories',
        name: 'categories',
        routes: categoriesRoute,
        layout: 'AdministrationLayout',
    },
    {
        pathPrefix: '/orders',
        name: 'orders',
        routes: ordersRoute,
        layout: 'AdministrationLayout',
    },
    {
        pathPrefix: '/invoices',
        name: 'invoices',
        routes: invoicesRoute,
        layout: 'AdministrationLayout',
    },
    {
        pathPrefix: '/users',
        name: 'users',
        routes: usersRoute,
        layout: 'AdministrationLayout',
    },
    {
        pathPrefix: '/calendar',
        name: 'calendar',
        routes: calendarRoute,
        layout: 'AdministrationLayout',
    },
];


export default routes.map(applicationMapper as any);
