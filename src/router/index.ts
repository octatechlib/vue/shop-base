import { createRouter, createWebHistory } from 'vue-router';
import type { RouteRecordRaw } from 'vue-router';

import pageRoutes from '@/router/pages';
import { storeFactory } from '@/main';

const router = createRouter({
    history: createWebHistory(`${import.meta.env.BASE_URL}`),
    routes: [...pageRoutes] as RouteRecordRaw[],
});

router.beforeEach((to, from) => {
    if (to.params.token && (to.name === 'authentication.activate' || to.name === 'authentication.reset-password')) {
        return true;
    }

    console.log('to', to.name);
    console.log('from', from.name);

    if (isAuthenticationRoute(to)) {
        console.log('is auth');
        // TODO: remove

        if (from.name !== undefined) {
            console.log('from auth');
            // TODO: remove
            return;
        }

        console.log('is auth2');
        return

        // TODO: remove

        if (!storeFactory?.userStore?.getToken) {
            router.push({ name: 'authentication.login' });
            return;
        }

        if (storeFactory?.userStore.hasValidToken) {
            router.push({ name: 'dashboard.index' });
            return;
        }
        console.log('is auth3');
        // TODO: remove
        return;

        storeFactory.userStore
            .refreshToken()
            .then(() => {
                router.push({ name: 'dashboard.index' });
            })
            .catch(() => {
                storeFactory.userStore.logout();
            });

        return;
    }

    if (!storeFactory?.userStore?.getToken) {
        router.push({ name: 'authentication.login' });
        return;
    }

    if (!storeFactory?.userStore.hasValidToken) {
        storeFactory.userStore.refreshToken().catch(() => {
            storeFactory.userStore.logout();
        });
        return;
    }
});

const isAuthenticationRoute = (to: any) => {
    const routes = ['authentication.login', 'authentication.forgot-password'];
    return routes.includes(to.name);
};

export default router;
