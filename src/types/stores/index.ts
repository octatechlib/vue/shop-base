/* eslint-disable prettier/prettier */
import type { Store } from 'pinia'
import type {
  User,
  Permission,
  PermissionIndexResponse,
  Role,
  RoleState
} from '@/types/stores/userStoreTypes'
import type { ConfigStoreTypes } from '@/types/stores/configStoreTypes'
import type { Methods } from '@/enums/apiEnum'
import type {
  UserState,
  PermissionState,
  TypeState,
  ValidationState, CountryState, ProductState, OrderState, CategoryState
} from '@/types/stores/states'
import type { ValidateResponse } from '@/Interfaces/api/Validation'
import type { AddressRequest, AddressResource, AddressState } from '@/types/stores/addressStoreTypes'
import type { Country } from '@/types/stores/countryStoreTypes'
import type { ProductIndexResponse } from '@/types/stores/productStoreTypes'

export type AddressStore = Store<
  'addressStore',
  AddressState,
  {
    getAddressById(state: AddressState): (id: number) => AddressResource | undefined;
    getAddress(state: AddressState): () => AddressResource;
    getAddresses(state: AddressState): () => AddressResource[];
  },
  {
    setAddress(data: AddressRequest): Promise<void>;
    searchAddresses(searchData: any): Promise<any>;
    search(entityId: number): Promise<any>;
    create(data: any): Promise<boolean>;
    createPending(data: any): Promise<boolean>;
    update(entityId: number, data: any): Promise<void>;
    updatePending(entityId: number, data: any): Promise<void>;
    restore(entityId: number): Promise<void>;
    remove(entityId: number): Promise<void>;
  }
>;

export type ConfigStore = Store<
  'configStore',
  ConfigStoreTypes,
  {
    colorSwatchPrimary(): any;
    light(): any;
    dark(): any;
  },
  {
    setSwatchColors(primary: any, secondary: any): void;
    setThemeMode(mode: string): void;
    setLocale(locale: string): void;
    setLocale(locale: string): void;
  }
>;

export type CategoryStore = Store<
  'categoryStore',
  CategoryState,
  {
    colorSwatchPrimary(): any;
    light(): any;
    dark(): any;
  },
  {
    setSwatchColors(primary: any, secondary: any): void;
    setThemeMode(mode: string): void;
    setLocale(locale: string): void;
    setLocale(locale: string): void;
  }
>;

export type CountryStore = Store<
  'countryStore',
  CountryState,
  {
    getCountries(state: CountryState): () => Country[];
    getCountry(state: CountryState): () => Country;
    getCountryById(state: CountryState): (id: number) => CountryState | Country | undefined;
  },
  {
    setCountry(country: Country): Promise<void>;
    searchCountries(): Promise<any>;
  }
>;


export type ModalStore = Store<
  'modalStore',
  any,
  {},
  {
    show(notification: any): void;
    removeModal(id: any): void;
  }
>;

export type PermissionStore = Store<
  'permissionStore',
  PermissionState,
  {
    hasPermissionByNameAndMethod(state: PermissionState): (name: string, method: Methods) => boolean;
    getPermissions(): Permission[];
  },
  {
    search(search: any): Promise<PermissionIndexResponse | []>;
    hasPermission(permission: string, method: string): boolean;
    setPermissions(permissions: any): void;
  }
>;

export type ProductStore = Store<
  'productStore',
  ProductState,
  {
    getPermissions(): Permission[];
  },
  {
    search(search: any): Promise<ProductIndexResponse | []>;
  }
>;

export type OrderStore = Store<
  'orderStore',
  OrderState,
  {
    getPermissions(): Permission[];
  },
  {
    search(search: any): Promise<ProductIndexResponse | []>;
  }
>;

export type RoleStore = Store<
  'roleStore',
  RoleState,
  {
    getRoleById(state: RoleState): (id: number) => Role | undefined;
    getRole(state: RoleState): () => Role;
    getRoles(state: RoleState): () => Role[];
  },
  {
    setRole(role: Role): Promise<void>;
    searchRoles(id: number, searchData: any): Promise<any>;
    searchRole(userId: number, roleId: number): Promise<any>;
    create(id: number, data: any): Promise<boolean | Role>;
    update(userId: number, roleId: number, data: any): Promise<boolean | Role>;
    fetchUserPermissions(): Promise<Permission[] | {}>;
    deleteRole(userId: number, roleId: number): Promise<void>;
  }
>;

export type TypeStore = Store<
  'typesStore',
  TypeState,
  {
    getAddressTypes(state: TypeState): () => any;
    getEmailTypes(state: TypeState): () => any;
    getVatTypes(state: TypeState): () => any;
    getTypes(state: TypeState): () => any;
  },
  {
    mapStoredTypes(type: string): () => any;
    searchTypes(type: string): Promise<any>;
    searchTypesForDropdown(type: string): Promise<any>;
  }
>;

export type ToastStore = Store<
  'toastStore',
  any,
  {},
  {
    removeNotification(id: any): void;
    success(title: string, message: string, config?: any): void;
    show(notification: any): void;
    warning(title: string, message: string, config?: any): void;
    danger(title: string, message: string, config?: any): void;
    info(title: string, message: string, config?: any): void;
    errorResponseToast(error: any): void;
  }
>;

export type UserStore = Store<
  'userStore',
  UserState,
  {
    getRefreshToken(): string;
    getLoggedInUser(): User;
    hasPermissionByNameAndMethod(state: UserState): (name: string, method: Methods) => boolean;
    hasValidToken(): boolean;
    getUsers(state: UserState): User[];
    getUser(state: UserState): User;
    getUserById(state: UserState): (userId: number) => User | undefined;
    getCurrentUser(): User;
    getPermissions(): Permission[];
    getToken(): string;
  },
  {
    setUser(user: User): Promise<void>;
    setCurrentUser(connectedUser: User): void;
    create(data: any): Promise<boolean | User>;
    update(id: number, data: any): Promise<boolean>;
    search(userId: number): Promise<any>;
    searchUsers(search: any): Promise<any>;
    searchUserById(id: number): Promise<any>;
    isActivated(email: string): Promise<unknown>;
    activate(id: number): Promise<void>;
    deactivate(id: number): Promise<void>;
    login(email: string, password: string): Promise<boolean>;
    logout(): Promise<any>;
    resetPassword(email: string): Promise<boolean>;
    refreshTokenOnError(errorRequest: any, resolve: any, reject: any): void;
    refreshToken(): Promise<unknown>;
    setToken(token?: string): void;
    setRefreshToken(token?: string): void;
    fetchRoles(): Promise<Role[] | []>;
    fetchPermissions(): Promise<void>;
    setPermissions(permissions: any): void;
    hasPermission(permission: string, method: string): boolean;
  }
>;

export type ValidationStore = Store<
  'validationStore',
  ValidationState,
  {},
  {
    validateAddress(data: any): Promise<ValidateResponse>;
    validateCompany(data: any): Promise<ValidateResponse>;
  }
>;

export type StoreFactories = {
  addressStore: AddressStore;
  categoryStore: CategoryStore;
  configStore: ConfigStore;
  modalStore: ModalStore;
  permissionStore: PermissionStore;
  productStore: ProductStore;
  orderStore: OrderStore;
  roleStore: RoleStore;
  toastStore: ToastStore;
  typeStore: TypeStore;
  userStore: UserStore;
  validationStore: ValidationStore;
};
