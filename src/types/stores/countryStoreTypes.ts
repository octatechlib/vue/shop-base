import type { ApiIndexResponse } from '@/types/clients/Response';

export type Country = {
    id: number;
    name: string;
    nationality: string;
    slug: string;
    iso: string;
    language_tag: string;
    label: string;
};

export type CountryIndexResponse = {
    data: Country[];
} & ApiIndexResponse;
