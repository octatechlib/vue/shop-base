import type { IndexLinksResponse, IndexMetaResponse, ApiIndexResponse } from '@/types/clients/Response';
import type { EntityStatusEnum } from '@/enums/entityStatusEnum';

type EntityStatuses = {
    _entity_status?: EntityStatusEnum | string | undefined;
};

type Timestamps = {
    deleted_at?: string | null | undefined;
};

export type AddressState = {
    addresses: AddressResource[];
    address: AddressResource;
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
};

export type AddressRequest = {
    country_id: number | undefined;
    type: string | undefined;
    street: string | undefined;
    house_number: number | undefined;
    house_number_extension: string | null | undefined;
    city: string | undefined;
    post_code: string | undefined;
};

export type AddressResource = {
    id: number | undefined;
    type: string;
    street: string;
    street_number: number;
    street_number_extension: string | null | undefined;
    city: string;
    post_code: string;
    country_id: number;
} & Timestamps;

export type AddressEntityStated = AddressResource & EntityStatuses;

export type AddressIndexResponse = {
    data: AddressResource[];
} & ApiIndexResponse;
