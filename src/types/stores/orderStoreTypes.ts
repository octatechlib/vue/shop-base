import type { ApiIndexResponse } from '@/types/clients/Response';

export type Order = {
  id: number;
  number: string;
}; // TODO: to finish

export type OrderIndexResponse = {
  data: Order[];
} & ApiIndexResponse;
