import type { CustomColorPalette } from '@/types/helpers/swatchGeneratorTypes';
import type { RemovableRef } from '@vueuse/core';
export type Theme = {
    mode: RemovableRef<string>;
    color: {
        swatch: {
            primary: CustomColorPalette;
            secondary: CustomColorPalette;
        };
        primary: string;
        secondary: string;
    };
};

export type ConfigStoreTypes = {
    theme: Theme;
    locale: RemovableRef<string>;
    clientSettings: {
        name: string;
    };
    apiValidationSettings: {
        isIgnoringAddressApi: boolean;
    };
};
