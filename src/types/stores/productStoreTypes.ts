import type { ApiIndexResponse } from '@/types/clients/Response';

export type Product = {
  id: number;
  name: string;
  category_id: string | null | undefined;
  slug: string;
}; // TODO: to finish

export type ProductIndexResponse = {
  data: Product[];
} & ApiIndexResponse;
