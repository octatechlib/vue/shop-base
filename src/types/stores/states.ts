import type { IndexLinksResponse, IndexMetaResponse } from '@/types/clients/Response';
import type { Country } from '@/types/stores/countryStoreTypes'
import type { Product } from '@/types/stores/productStoreTypes'
import type { User, Permission, Role } from '@/types/stores/userStoreTypes';
import type { Category } from '@/types/stores/categoryStoreTypes'
import type { Order } from '@/types/stores/orderStoreTypes'

export type CategoryState = {
    categories: Category[];
    category: Category;
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
};


export type CountryState = {
    countries: Country[];
    country: Country;
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
};

export type PermissionState = {
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
    permissions: Permission[];
};

export type ProductState = {
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
    products: Product[];
    product: Product;
};

export type OrderState = {
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
    orders: Order[];
    order: Order;
};

export type TypeState = {
    types: string[] | {}
}

export type UserState = {
    users: User[];
    user: User;
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
    permissions: Permission[];
    current_user: User;
    token: string;
    refresh_token: string;
    roles: Role[];
};

export type ValidationState = {
    data: {};
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
};

export type AddressTypeState = TypeState;
export type EmailTypeState = TypeState;
export type VatTypeTypeState = TypeState;
