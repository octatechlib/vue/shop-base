import type { ApiIndexResponse, IndexLinksResponse, IndexMetaResponse } from '@/types/clients/Response';

export type RoleState = {
    roles: Role[];
    role: Role;
    meta: IndexMetaResponse;
    links: IndexLinksResponse;
};

export type Role = {
    id: number;
    name: string;
    description: number;
};

export type UserIndexResponse = {
    data: User[];
} & ApiIndexResponse;

export type PermissionIndexResponse = {
    data: Permission[];
} & ApiIndexResponse;

export type Permission = {
    id: number;
    name: string;
    description: string;
    method: string;
};

export type User = {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    language_iso_code: string;
    type: string;
    created_at: string | null;
    updated_at: string | null;
    deleted_at?: string | null;
    roles?: Role[];
    company_branch_id: number;
    activity_status: string | null;
};
