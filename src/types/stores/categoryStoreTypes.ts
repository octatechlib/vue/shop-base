import type { ApiIndexResponse } from '@/types/clients/Response';

export type Category = {
  id: number;
  name: string;
  slug: string;
}; // TODO: to finish

export type CategoryIndexResponse = {
  data: Category[];
} & ApiIndexResponse;
