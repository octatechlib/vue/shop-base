export type RefreshToken =
    | {
          company_id: number;
          company_branch_id?: number;
      }
    | {};
