export type ApiIndexResponse = {
    links: IndexLinksResponse;
    meta: IndexMetaResponse;
    status: number;
};

export type IndexMetaResponse = {
    current_page: number;
    from: number;
    last_page: number;
    links: IndexMetaLinksResponse;
    path: string;
    per_page: number;
    to: number;
    total: number;
};

export type IndexLinksResponse = {
    first: string;
    last: string;
    next: string | null;
    prev: string | null;
};

export type IndexMetaLinksResponse = {
    active: boolean;
    label: string;
    url: string;
};
