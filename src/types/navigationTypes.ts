export interface MenuItem {
    title: string;
    to: {
        name: string;
    };
    external?: boolean;
    isAllowed: boolean;
}

export type MenuCategory = {
    title: string;
    icon: string;
    children: MenuItem[] | MenuCategory[];
    open?: boolean;
    to: {
        name: string;
    };
    isAllowed: boolean;
};
