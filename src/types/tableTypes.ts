import type { ComputedRef } from 'vue';

export type TablePagination = {
    current: number;
    pageSize: number;
    total: number;
};

export type TableSorter = {
    order: 'ascend' | 'descend';
    field: string;
    columnKey: string | null;
};

export type VuetifyDataTableServer = {
    itemsPerPage: number;
    headers: ComputedRef<
        Array<{
            title: string;
            align: string;
            sortable: boolean;
            key: string;
        }>
    >;
    serverItems: any[];
    loading: boolean;
    totalItems: number;
    search: {
        [key: string]: any;
        type: string;
    };
    page: number;
    actions: TableActions;
    itemsPerPageOptions?: Array<{ title: string; value: number }>;
};

export type VuetifyPagination = {
    groupBy: any;
    itemsPerPage: number;
    page: number;
    search: string | number;
    sortBy: VuetifyPaginationSortBy[];
};

export type VuetifyPaginationSortBy = {
    key: string;
    order: string;
};

export type TableActions = ComputedRef<{ [key: string]: TableAction } | {}>;

export type TableAction = {
    title: string;
    icon: string;
    process: Function;
    isAllowed: boolean;
};
