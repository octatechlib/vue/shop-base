import type { MenuCategory } from '@/types/navigationTypes';
import { MenuGuard } from '@/guards/MenuGuard';
import { ObjectHelper } from '@/helpers/ObjectHelper';

const menuItems = (): MenuCategory[] => {
    const menuGuard = new MenuGuard();
    // const objectHelper = new ObjectHelper();

    return new ObjectHelper().filterAllowed(
        [
            {
                title: 'menu.dashboard.title',
                icon: 'mdi-view-dashboard-outline',
                isAllowed: true,
                to: {
                    name: 'dashboard.index',
                },
                children: [],
            },
            {
                title: 'menu.products.title',
                icon: 'mdi-atv',
                isAllowed: true,//menuGuard.hasUserViewPermission(),
                to: {},
                children: [
                  {
                    title: 'menu.products.children.list.title',
                    icon: 'mdi-list-box-outline',
                    to: {
                      name: 'products.index',
                    },
                    isAllowed: true
                  },
                  {
                    title: 'menu.products.children.in_stock.title',
                    icon: 'mdi-book-check-outline',
                    to: {
                      name: 'products.in-stock',
                    },
                    isAllowed: true
                  },
                  {
                    title: 'menu.products.children.out_of_stock.title',
                    icon: 'mdi-book-clock-outline',
                    to: {
                      name: 'products.out-of-stock',
                    },
                    isAllowed: true
                  },
                  {
                    title: 'menu.products.children.discounts.title',
                    icon: 'mdi-tag-arrow-down',
                    to: {
                      name: 'products.discounts',
                    },
                    isAllowed: true
                  }
                ],
            },
            {
                title: 'menu.categories.title',
                icon: 'mdi-format-list-text',
                isAllowed: true,//menuGuard.hasUserViewPermission(),
                to: {
                    name: 'categories.index',
                },
                children: [],
            },
            {
                title: 'menu.orders.title',
                icon: 'mdi-gesture-tap-button',
                isAllowed: true,//menuGuard.hasUserViewPermission(),
                to: {},
                children: [
                  {
                    title: 'menu.orders.children.list.title',
                    icon: 'mdi-format-list-checks',
                    to: {
                      name: 'orders.index',
                    },
                    isAllowed: true
                  },
                  {
                    title: 'menu.orders.children.pending.title',
                    icon: 'mdi-clock-end',
                    to: {
                      name: 'orders.pending',
                    },
                    isAllowed: true
                  }
                ],
            },
            {
                title: 'menu.invoices.title',
                icon: 'mdi-invoice-text-edit-outline',
                isAllowed: true,//menuGuard.hasUserViewPermission(),
                to: {},
                children: [
                  {
                    title: 'menu.invoices.children.list.title',
                    icon: 'mdi-format-list-text',
                    to: {
                      name: 'invoices.index',
                    },
                    isAllowed: true
                  },
                  {
                    title: 'menu.invoices.children.new.title',
                    icon: 'mdi-invoice-text-plus',
                    to: {
                      name: 'invoices.new',
                    },
                    isAllowed: true
                  }
                ],
            },
            {
                title: 'menu.users.title',
                icon: 'mdi-account-multiple-outline',
                isAllowed: true,//menuGuard.hasUserViewPermission(),
                to: {
                    name: 'users.index',
                },
                children: [
                  {
                    title: 'menu.users.children.active.title',
                    icon: 'mdi-account-group',
                    to: {
                      name: 'users.active',
                    },
                    isAllowed: true
                  },
                  {
                    title: 'menu.users.children.inactive.title',
                    icon: 'mdi mdi-account-off',
                    to: {
                      name: 'users.inactive',
                    },
                    isAllowed: true
                  }
                ],
            },
            {
              title: 'menu.calendar.title',
              icon: 'mdi-calendar-month-outline',
              isAllowed: true,//menuGuard.hasUserViewPermission(),
              to: {
                name: 'calendar.index',
              },
              children: [],
            },
        ] as MenuCategory[],
        []
    ) as MenuCategory[];
};

export { menuItems };
