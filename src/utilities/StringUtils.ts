export function extractAndRemoveParamsInBraces(inputString: string): {
    cleanedString: string;
    squareBrackets: string;
    curlyBraces: string[];
    extractedNumbers: number[];
} {
    ///example API response string "[max].password {45}"

    // Extract text between square brackets
    const squareBrackets = (inputString.match(/\[(.*?)\]/g) || [])
        .map((match) => match.slice(1, -1))
        .join('')
        .trim();

    // Extract text between curly braces
    // Extract text between curly braces and convert to an array of numbers
    const curlyBracesMatches = inputString.match(/\{(.*?)\}/g) || [];
    const curlyBraces = curlyBracesMatches.map((match) => match.slice(1, -1));

    // Remove square brackets and curly braces from the input string and remove numbers
    const cleanedString = (
        squareBrackets +
        inputString
            .replace(/\[.*?\]/g, '')
            .replace(/\{.*?\}/g, '')
            .replace(/\d+\./g, '')
    ).trim();

    // Helper function to extract numbers from a string
    const extractNumbers = (str: string): number[] => {
        return (str.match(/\d+/g) || []).map((num) => Number(num) + 1);
    };

    // Extract numbers from the cleaned string
    const extractedNumbers = extractNumbers(inputString);

    return { cleanedString, squareBrackets, curlyBraces, extractedNumbers };
}

export function ucfirst(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1);
}
