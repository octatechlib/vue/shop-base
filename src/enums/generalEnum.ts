export enum ActivityStatusEnum {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
    CREATED = 'created',
    QUEUED = 'queued',
    PENDING = 'pending',
    FAILED = 'failed',
    SYNCED = 'synced',
}

export type ValueOf<T> = T[keyof T];
