export enum ValidationStatusEnum {
    STATUS_VALID = 'valid',
    STATUS_INVALID = 'invalid',
    STATUS_NO_RESPONSE = 'no_response',
}
