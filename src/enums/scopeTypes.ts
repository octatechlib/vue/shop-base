export enum ScopeTypesEnum {
    INTERNAL = 'internal',
    EXTERNAL = 'external',
}
