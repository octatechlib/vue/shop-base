export enum Api {
    Auth = 'auth',
    Payroll = 'payroll',
}

export enum Methods {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    PATCH = 'PATCH',
    DELETE = 'DELETE',
}

export enum Types {
    ADDRESS = 'address',
    EMAIL = 'email',
    ORGANISATION = 'organisation_unit',
    VAT_TYPES = 'vat_types',
}
