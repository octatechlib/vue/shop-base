export enum WageComponentTypeEnum {
    INTERVAL = 'interval',
    ALLOWANCE = 'allowance',
}
