export enum EntityStatusEnum {
    NEW = 'new',
    ORIGINAL = 'original',
    CHANGED = 'changed',
}
