export enum ActivityStatusEnum {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
}
