export enum RelationsEnum {
    EXTERNAL_SERVICE = 'externalService',
    EMAILS = 'emails',
    USER = 'user',
    ROLES = 'roles',
    EMPLOYEE = 'employee',
    EMPLOYER_COMPANY = 'employerCompany',
    INTERMEDIARY_COMPANY = 'intermediaryCompany',
}
