export enum BranchTypesEnum {
    EMPLOYER = 'employer',
    INTERMEDIARY = 'intermediary',
    ENTITY = 'entity',
    PAYROLL = 'payroll',
}

export enum CompanyRelationsEnum {
    ADDRESSES = 'addresses',
    CONTACTS = 'contacts',
    BRANCHES = 'companyBranches',
    ORGANISATION_UNITS = 'organisationUnits',
    INTERMEDIARY_BRANCH_ORGANISATION_UNITS = 'intermediaryBranchOrganisationUnits',
    ENTITY_ORGANISATION_UNITS = 'entityBranchOrganisationUnits',
    MANAGED_COMPANIES = 'managedCompanies',
    MANAGED_BY_COMPANIES = 'managedByCompanies',
    COMPANY_CLASSIFICATIONS = 'companyClassifications',
}
