export enum TypesEnum {
    TYPE_ADMIN = 'admin',
    TYPE_COMPANY = 'company',
    TYPE_EMPLOYEE = 'employee',
}

export enum ActivityStatusEnum {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
}

export type ValueOf<T> = T[keyof T];
