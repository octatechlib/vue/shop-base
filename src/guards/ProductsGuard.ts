import { Methods } from '@/enums/apiEnum';
import { storeFactory } from '@/main';

export class ProductsGuard {
    canView() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('products_list', Methods.GET);
    }
    canShow() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('products_show', Methods.GET);
    }

    canCreate() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('products_create', Methods.POST);
    }

    canUpdate() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('products_update', Methods.PUT);
    }

    canSeeOutOfStock() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('products_list', Methods.GET);
    }

    canSeeDiscounts() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('products_list', Methods.GET);
    }
}
