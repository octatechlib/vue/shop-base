import { Methods } from '@/enums/apiEnum';
import { storeFactory } from '@/main';

export class MenuGuard {
    hasUserViewPermission() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_users', Methods.GET);
    }
}
