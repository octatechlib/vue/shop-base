import { UserGuard } from '@/guards/UserGuard'

export class GeneralGuard {
    userGuard: UserGuard;

    constructor() {
        this.userGuard = new UserGuard();
    }

    canEnterMenuItem(routeData: { name: string }) {
        switch (routeData.name) {
            case 'dashboard.index': {
                return true; // as long as logged in
            }
            case 'users.view': {
                return this.userGuard.canView();
            }
            default: {
                return true; //TODO:: When this is finish block the other menu stuff
            }
        }
    }
    canEnterMenuItemFrom(currentPage: { name: string }, targetPage: { name: string }) {
        console.log('TODO: to implement', currentPage, targetPage);
    }
}
