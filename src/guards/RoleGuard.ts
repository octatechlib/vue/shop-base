import { Methods } from '@/enums/apiEnum';
import { storeFactory } from '@/main';

export class RoleGuard {
    canView() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_roles', Methods.GET);
    }

    canCreate() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_roles', Methods.POST);
    }

    canUpdate() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_roles', Methods.PUT);
    }

    canDelete() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_roles', Methods.DELETE);
    }
}
