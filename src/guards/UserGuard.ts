import { Methods } from '@/enums/apiEnum';
import { storeFactory } from '@/main';

export class UserGuard {
    canView() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_users', Methods.GET);
    }

    canShow(): boolean {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_users', Methods.GET);
    }

    canCreate() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_users', Methods.POST);
    }

    canUpdate() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_users', Methods.PUT);
    }

    canDelete() {
        return storeFactory.userStore.hasPermissionByNameAndMethod('auth_service_users', Methods.DELETE);
    }
}
